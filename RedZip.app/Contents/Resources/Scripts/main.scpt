FasdUAS 1.101.10   ��   ��    k             l      ��  ��   ��Red Zipping Script
Functions of this script:
1. select a Job folder - this should be the top level folder which contains the Reels folder. 
2. Finds Red Reels and thus Red RDC folders
3. Zips RDC folders with no compression
4. Moves RDC folders into unzipped folder

version 3
�Nov 25th 2017
Rupert Howe
Head of Marketing and Business Development
Support Partners

Version Release Notes:
1. Created by Rupert, 14th Nov
2. Added error catching & messages
3. Added progress bar for clips in each Red reel, adjusted for no SOURCE folder and new Red filename

For Support with this script, please email
rupert.howe@support-partners.com

Users of this script do so at their own risk.
     � 	 	N R e d   Z i p p i n g   S c r i p t 
 F u n c t i o n s   o f   t h i s   s c r i p t : 
 1 .   s e l e c t   a   J o b   f o l d e r   -   t h i s   s h o u l d   b e   t h e   t o p   l e v e l   f o l d e r   w h i c h   c o n t a i n s   t h e   R e e l s   f o l d e r .   
 2 .   F i n d s   R e d   R e e l s   a n d   t h u s   R e d   R D C   f o l d e r s 
 3 .   Z i p s   R D C   f o l d e r s   w i t h   n o   c o m p r e s s i o n 
 4 .   M o v e s   R D C   f o l d e r s   i n t o   u n z i p p e d   f o l d e r 
 
 v e r s i o n   3 
 � N o v   2 5 t h   2 0 1 7 
 R u p e r t   H o w e 
 H e a d   o f   M a r k e t i n g   a n d   B u s i n e s s   D e v e l o p m e n t 
 S u p p o r t   P a r t n e r s 
 
 V e r s i o n   R e l e a s e   N o t e s : 
 1 .   C r e a t e d   b y   R u p e r t ,   1 4 t h   N o v 
 2 .   A d d e d   e r r o r   c a t c h i n g   &   m e s s a g e s 
 3 .   A d d e d   p r o g r e s s   b a r   f o r   c l i p s   i n   e a c h   R e d   r e e l ,   a d j u s t e d   f o r   n o   S O U R C E   f o l d e r   a n d   n e w   R e d   f i l e n a m e 
 
 F o r   S u p p o r t   w i t h   t h i s   s c r i p t ,   p l e a s e   e m a i l 
 r u p e r t . h o w e @ s u p p o r t - p a r t n e r s . c o m 
 
 U s e r s   o f   t h i s   s c r i p t   d o   s o   a t   t h e i r   o w n   r i s k . 
   
  
 l     ��������  ��  ��        j     �� �� 0 apptitle appTitle  m        �    F P L   P O C   R e d Z i p      l     ��������  ��  ��        l     ��������  ��  ��        l     ��������  ��  ��        p       ������  0 unpackedfolder unpackedFolder��        p       ������ $0 sourcefoldername sourceFolderName��        p       ������ 0 clips001name clips001Name��       !   p     " " ������ 0 	jobfolder 	jobFolder��   !  # $ # p     % % ������ 0 thereels theReels��   $  & ' & p     ( ( ������ 0 thereel theReel��   '  ) * ) p     + + ������ 0 thereelfolder theReelFolder��   *  , - , p     . . ������ (0 thereelfolderalias theReelFolderAlias��   -  / 0 / p     1 1 ������ 0 thereelalias theReelAlias��   0  2 3 2 p     4 4 ������ *0 thesourcefoldername theSourceFolderName��   3  5 6 5 p     7 7 ������ (0 unpackedfoldername unpackedFolderName��   6  8 9 8 l     ��������  ��  ��   9  : ; : l      �� < =��   < < 6 set sourceFolderName to "SOURCE"  - this was removed     = � > > l   s e t   s o u r c e F o l d e r N a m e   t o   " S O U R C E "     -   t h i s   w a s   r e m o v e d   ;  ? @ ? l     ��������  ��  ��   @  A B A l     C���� C r      D E D m      F F � G G  U N P A C K E D E o      ���� (0 unpackedfoldername unpackedFolderName��  ��   B  H I H l     ��������  ��  ��   I  J K J p     L L ������ 0 theclips theClips��   K  M N M p     O O ������ "0 thesourcefolder theSourceFolder��   N  P Q P p     R R ������ 0 theclip theClip��   Q  S T S p     U U ������ ,0 thesourcefolderalias theSourceFolderAlias��   T  V W V p     X X ������ 0 theclipname theClipName��   W  Y Z Y p     [ [ ������ *0 thesourcefolderpath theSourceFolderPath��   Z  \ ] \ p     ^ ^ ������ 0 theclipname theClipName��   ]  _ ` _ p     a a ������ 0 therdcfolders theRDCFolders��   `  b c b p     d d ������ "0 thesourcefolder theSourceFolder��   c  e f e p     g g ������ 0 therdcfolder theRDCFolder��   f  h i h p     j j ������ &0 therdcfolderalias theRDCFolderAlias��   i  k l k p     m m ������ $0 therdcfoldername theRDCFolderName��   l  n o n p     p p ������ $0 therdcfolderpath theRDCFolderPath��   o  q r q l     ��������  ��  ��   r  s t s l     ��������  ��  ��   t  u v u l    w���� w I   �� x y
�� .sysodlogaskr        TEXT x m     z z � { { T h i s   a p p l i c a t i o n   h u n t s   d o w n   R D C   f o l d e r s   i n   P O C   j o b s 
 a n d   m a k e s   u n c o m p r e s s e d   r 3 d z i p s   f r o m   t h e m . 
 
 Y o u   n e e d   t o   h a v e   m a d e   p r o x i e s   f i r s t .   R e a d y ? y �� | }
�� 
btns | J    
 ~ ~   �  m     � � � � �  N o t   q u i t e �  ��� � m     � � � � �  Y e s !��   } �� � �
�� 
dflt � m    ����  � �� � �
�� 
cbtn � m    ����  � �� ���
�� 
appr � o    ���� 0 apptitle appTitle��  ��  ��   v  � � � l     ��������  ��  ��   �  � � � l   w ����� � O    w � � � k    v � �  � � � l   ��������  ��  ��   �  � � � r    & � � � l   $ ����� � I   $���� �
�� .sysostflalis    ��� null��   � �� ���
�� 
prmp � m      � � � � � 4 C h o o s e   y o u r   P O C   J o b   F o l d e r��  ��  ��   � o      ���� 0 	jobfolder 	jobFolder �  � � � l  ' '��������  ��  ��   �  � � � r   ' 5 � � � c   ' 1 � � � l  ' - ����� � n   ' - � � � 4   ( -�� �
�� 
cfol � o   + ,���� (0 unpackedfoldername unpackedFolderName � o   ' (���� 0 	jobfolder 	jobFolder��  ��   � m   - 0��
�� 
alis � o      ����  0 unpackedfolder unpackedFolder �  � � � l  6 6��������  ��  ��   �  � � � r   6 F � � � n   6 B � � � 2   > B��
�� 
cobj � 4   6 >�� �
�� 
cfol � o   : =����  0 unpackedfolder unpackedFolder � o      ���� 0 thereels theReels �  � � � l  G G�������  ��  �   �  ��~ � X   G v ��} � � k   ] q � �  � � � r   ] f � � � c   ] b � � � o   ] ^�|�| 0 thereel theReel � m   ^ a�{
�{ 
alis � o      �z�z 0 thereelalias theReelAlias �  � � � l  g g�y�x�w�y  �x  �w   �  � � � l   g g�v � ��v   � � � Removed code for Source Folder here:
		set sourceFolder to (folder sourceFolderName of theReelAlias) as alias 		my folderSubRoutine(sourceFolder)     � � � �(   R e m o v e d   c o d e   f o r   S o u r c e   F o l d e r   h e r e : 
 	 	 s e t   s o u r c e F o l d e r   t o   ( f o l d e r   s o u r c e F o l d e r N a m e   o f   t h e R e e l A l i a s )   a s   a l i a s    	 	 m y   f o l d e r S u b R o u t i n e ( s o u r c e F o l d e r )   �  � � � l  g g�u�t�s�u  �t  �s   �  � � � l   g o � � � � n  g o � � � I   h o�r ��q�r $0 foldersubroutine folderSubRoutine �  ��p � o   h k�o�o 0 thereelalias theReelAlias�p  �q   �  f   g h � ( " new code for Reel without Source     � � � � D   n e w   c o d e   f o r   R e e l   w i t h o u t   S o u r c e   �  ��n � l  p p�m�l�k�m  �l  �k  �n  �} 0 thereel theReel � o   J M�j�j 0 thereels theReels�~   � m     � ��                                                                                  MACS  alis    t  Macintosh HD               �WZ�H+   G�
Finder.app                                                      �p���        ����  	                CoreServices    �WL�      ���     G� �H �F  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  ��  ��   �  � � � l     �i�h�g�i  �h  �g   �  � � � l  x � ��f�e � O   x � � � � k   | � � �  � � � I  | ��d � �
�d .sysodlogaskr        TEXT � m   |  � � � � � f A l l   R D C   c l i p   f o l d e r s   i n   t h i s   j o b   h a v e   b e e n   Z i p i f i e d � �c � �
�c 
btns � J   � � � �  ��b � m   � � � � � � � " H a v e   a   g r e a t   d a y !�b   � �a ��`
�a 
appr � o   � ��_�_ 0 apptitle appTitle�`   �  � � � r   � � � � � I  � ��^ � �
�^ .corecrel****      � null � m   � ��]
�] 
brow � �\ ��[
�\ 
to   � o   � ��Z�Z 0 	jobfolder 	jobFolder�[   � o      �Y�Y .0 newwindowprojectfiles newWindowProjectFiles �  � � � r   � � � � � m   � ��X
�X ecvwclvw � n       � � � 1   � ��W
�W 
pvew � o   � ��V�V .0 newwindowprojectfiles newWindowProjectFiles �  � � � I  � ��U�T�S
�U .miscactvnull��� ��� obj �T  �S   �  ��R � l  � ��Q�P�O�Q  �P  �O  �R   � m   x y � ��                                                                                  MACS  alis    t  Macintosh HD               �WZ�H+   G�
Finder.app                                                      �p���        ����  	                CoreServices    �WL�      ���     G� �H �F  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  �f  �e   �  � � � l     �N�M�L�N  �M  �L   �  � � � l  � � ��K�J � I  � ��I ��H
�I .aevtoappnull  �   � **** �  ;   � ��H  �K  �J   �    l     �G�F�E�G  �F  �E    l     �D�C�B�D  �C  �B    l     �A�@�?�A  �@  �?    i    	 I      �>
�=�> $0 foldersubroutine folderSubRoutine
 �< o      �;�; 0 thereelfolder theReelFolder�<  �=  	 k    �  r      c      o     �:�: 0 thereelfolder theReelFolder m    �9
�9 
alis o      �8�8 (0 thereelfolderalias theReelFolderAlias  r     l   	�7�6 n    	 1    	�5
�5 
psxp o    �4�4 (0 thereelfolderalias theReelFolderAlias�7  �6   o      �3�3 &0 thereelfolderpath theReelFolderPath  O     r     n     !  1    �2
�2 
pnam! o    �1�1 (0 thereelfolderalias theReelFolderAlias o      �0�0 &0 thereelfoldername theReelFolderName m    ""�                                                                                  MACS  alis    t  Macintosh HD               �WZ�H+   G�
Finder.app                                                      �p���        ����  	                CoreServices    �WL�      ���     G� �H �F  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��   #$# l   �/�.�-�/  �.  �-  $ %&% O    /'(' r    .)*) c    ,+,+ l   *-�,�+- 6   *./. n    !010 2    !�*
�* 
cfol1 4    �)2
�) 
cfol2 o    �(�( 0 thereelfolder theReelFolder/ D   " )343 1   # %�'
�' 
pnam4 m   & (55 �66  . R D C�,  �+  , m   * +�&
�& 
alst* o      �%�% 0 cliplist clipList( m    77�                                                                                  MACS  alis    t  Macintosh HD               �WZ�H+   G�
Finder.app                                                      �p���        ����  	                CoreServices    �WL�      ���     G� �H �F  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  & 898 l  0 0�$�#�"�$  �#  �"  9 :;: r   0 5<=< n   0 3>?> 1   1 3�!
�! 
leng? o   0 1� �  0 cliplist clipList= o      �� 0 thezipcount theZipCount; @A@ r   6 ;BCB o   6 7�� 0 thezipcount theZipCountC 1   7 :�
� 
ppgtA DED r   < AFGF m   < =��  G 1   = @�
� 
ppgcE HIH O  B LJKJ I  F K���
� .miscactvnull��� ��� obj �  �  K  f   B CI LML r   M TNON b   M PPQP m   M NRR �SS  Z i p p i n g   R e e l  Q o   N O�� &0 thereelfoldername theReelFolderNameO 1   P S�
� 
ppgdM TUT r   U \VWV m   U VXX �YY " P r e p a r i n g   t o   z i p .W 1   V [�
� 
ppgaU Z[Z r   ] `\]\ m   ] ^��  ] o      �� 0 a  [ ^_^ l  a a����  �  �  _ `a` X   a|b�cb k   uwdd efe l  u u����  �  �  f ghg r   u ziji [   u xklk o   u v�� 0 a  l m   v w�
�
 j o      �	�	 0 a  h mnm I  { ��o�
� .sysodelanull��� ��� nmbro m   { |�� �  n pqp O   � �rsr k   � �tt uvu r   � �wxw c   � �yzy o   � ��� 0 therdcfolder theRDCFolderz m   � ��
� 
alisx o      �� &0 therdcfolderalias theRDCFolderAliasv {�{ r   � �|}| l  � �~�� ~ n   � �� 1   � ���
�� 
pnam� o   � ����� 0 therdcfolder theRDCFolder�  �   } o      ���� $0 therdcfoldername theRDCFolderName�  s m   � ����                                                                                  MACS  alis    t  Macintosh HD               �WZ�H+   G�
Finder.app                                                      �p���        ����  	                CoreServices    �WL�      ���     G� �H �F  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  q ��� r   � ���� b   � ���� b   � ���� b   � ���� b   � ���� m   � ��� ��� 0 Z i p p i n g   R D C   c l i p   f o l d e r  � o   � ����� 0 a  � m   � ��� ���    o f  � o   � ����� 0 thezipcount theZipCount� m   � ��� ���    i n   t h i s   r e e l� 1   � ���
�� 
ppga� ��� O   �k��� k   �j�� ��� r   � ���� l  � ������� n   � ���� 1   � ���
�� 
psxp� o   � ����� &0 therdcfolderalias theRDCFolderAlias��  ��  � o      ���� $0 therdcfolderpath theRDCFolderPath� ��� r   � ���� c   � ���� b   � ���� o   � ����� 0 thereelalias theReelAlias� m   � ��� ��� & R D C _ F O L D E R S _ P R E Z I P :� m   � ���
�� 
TEXT� o      ���� 00 therdcfoldertidystring theRDCFolderTidyString� ��� r   � ���� b   � ���� b   � ���� o   � ����� &0 thereelfolderpath theReelFolderPath� o   � ����� $0 therdcfoldername theRDCFolderName� m   � ��� ���  . r 3 d z i p� o      ���� &0 ther3dzipfilepath theR3DZipFilePath� ��� Q   �%���� r   � ���� l  � ������� I  � ������
�� .sysoexecTEXT���     TEXT� b   � ���� b   � ���� b   � ���� m   � ��� ���  z i p   - r 0  � n   � ���� 1   � ���
�� 
strq� o   � ����� &0 ther3dzipfilepath theR3DZipFilePath� m   � ��� ���   � n   � ���� 1   � ���
�� 
strq� o   � ����� $0 therdcfolderpath theRDCFolderPath��  ��  ��  � o      ���� 0 zipfilescript zipFileScript� R      �����
�� .ascrerr ****      � ****� o      ���� 0 theerror theError��  � k   �%�� ��� I  �#����
�� .sysodlogaskr        TEXT� b   ���� b   ���� b   � ���� m   � ��� ���  T h e   R D C   f o l d e r  � o   � ����� 0 rdcfoldername RDCFolderName� m   ��� ��� � f a i l e d   t o   z i p 
 	 
 P l e a s e   d o   n o t   p r o c e e d   -   s t o p   h e r e ,   d e l e t e   t h i s   z i p   f i l e ,   r e s e t   a n d   r e s t a r t . 
 
 E r r o r   c o d e :  � o  ���� 0 theerror theError� ����
�� 
btns� J  �� ���� m  �� ���  E x i t��  � ����
�� 
dflt� m  ���� � ����
�� 
cbtn� m  ���� � �����
�� 
appr� o  ���� 0 apptitle appTitle��  � ���� l $$��������  ��  ��  ��  � ��� Z  &T������� H  &3�� l &2������ I &2�����
�� .coredoexnull���     obj � l &.������ n  &.��� 4  ).���
�� 
cfol� m  *-�� ��� $ R D C _ F O L D E R S _ P R E Z I P� o  &)���� 0 thereelalias theReelAlias��  ��  ��  ��  ��  � I 6P�����
�� .corecrel****      � null��  � ����
�� 
kocl� m  :;��
�� 
cfol� ����
�� 
insh� o  >A���� 0 thereelalias theReelAlias� �����
�� 
prdt� K  DJ�� �����
�� 
pnam� m  EH�� ��� $ R D C _ F O L D E R S _ P R E Z I P��  ��  ��  ��  � ��� I Uh�� 
�� .coremovenull���     obj   4  UY��
�� 
cfol o  WX���� 0 therdcfolder theRDCFolder ����
�� 
insh l \d���� n  \d 4  _d��
�� 
cfol m  `c �		 $ R D C _ F O L D E R S _ P R E Z I P o  \_���� 0 thereelalias theReelAlias��  ��  ��  � 
��
 l ii��������  ��  ��  ��  � m   � ��                                                                                  MACS  alis    t  Macintosh HD               �WZ�H+   G�
Finder.app                                                      �p���        ����  	                CoreServices    �WL�      ���     G� �H �F  6Macintosh HD:System: Library: CoreServices: Finder.app   
 F i n d e r . a p p    M a c i n t o s h   H D  &System/Library/CoreServices/Finder.app  / ��  �  l ll��������  ��  ��    r  lq o  lm���� 0 a   1  mp��
�� 
ppgc �� I rw����
�� .sysodelanull��� ��� nmbr m  rs���� ��  ��  � 0 therdcfolder theRDCFolderc o   d e���� 0 cliplist clipLista  l }}��������  ��  ��    l }}��������  ��  ��    l }}��������  ��  ��    l }}����   %  Reset the progress information    � >   R e s e t   t h e   p r o g r e s s   i n f o r m a t i o n   r  }�!"! m  }~����  " o      ���� 0 a    #$# r  ��%&% m  ������  & 1  ����
�� 
ppgt$ '(' r  ��)*) m  ������  * 1  ����
�� 
ppgc( +,+ r  ��-.- m  ��// �00  . 1  ����
�� 
ppgd, 121 r  ��343 m  ��55 �66  4 1  ����
�� 
ppga2 7��7 l ����������  ��  ��  ��   8��8 l     ��������  ��  ��  ��       ��9 :; F������  9 �������������� 0 apptitle appTitle�� $0 foldersubroutine folderSubRoutine
�� .aevtoappnull  �   � ****�� (0 unpackedfoldername unpackedFolderName��  ��  : ��	����<=���� $0 foldersubroutine folderSubRoutine�� ��>�� >  �� 0 thereelfolder theReelFolder��  < �~�}�|�{�z�y�x�w�v�u�t�s�~ 0 thereelfolder theReelFolder�} &0 thereelfolderpath theReelFolderPath�| &0 thereelfoldername theReelFolderName�{ 0 cliplist clipList�z 0 thezipcount theZipCount�y 0 a  �x 0 therdcfolder theRDCFolder�w 00 therdcfoldertidystring theRDCFolderTidyString�v &0 ther3dzipfilepath theR3DZipFilePath�u 0 zipfilescript zipFileScript�t 0 theerror theError�s 0 rdcfoldername RDCFolderName= 9�r�q�p"�o�n?5�m�l�k�j�iR�hX�g�f�e�d�c�b�a����`�_��^���]��\�[�Z���Y��X�W�V�U�T��S�R�Q��P�O�N/5
�r 
alis�q (0 thereelfolderalias theReelFolderAlias
�p 
psxp
�o 
pnam
�n 
cfol?  
�m 
alst
�l 
leng
�k 
ppgt
�j 
ppgc
�i .miscactvnull��� ��� obj 
�h 
ppgd
�g 
ppga
�f 
kocl
�e 
cobj
�d .corecnte****       ****
�c .sysodelanull��� ��� nmbr�b &0 therdcfolderalias theRDCFolderAlias�a $0 therdcfoldername theRDCFolderName�` $0 therdcfolderpath theRDCFolderPath�_ 0 thereelalias theReelAlias
�^ 
TEXT
�] 
strq
�\ .sysoexecTEXT���     TEXT�[ 0 theerror theError�Z  
�Y 
btns
�X 
dflt
�W 
cbtn
�V 
appr�U 
�T .sysodlogaskr        TEXT
�S .coredoexnull���     obj 
�R 
insh
�Q 
prdt�P 
�O .corecrel****      � null
�N .coremovenull���     obj �����&E�O��,E�O� ��,E�UO� *�/�-�[�,\Z�?1�&E�UO��,E�O�*�,FOj*�,FO) *j UO��%*�,FO�*a ,FOjE�O�[a a l kh �kE�Okj O� ��&E` O��,E` UOa �%a %�%a %*a ,FO� �_ �,E` O_ a %a &E�O�_ %a %E�O  a �a  ,%a !%_ a  ,%j "E�W 2X # $a %�%a &%�%a 'a (kva )ka *ka +b   a , -OPO_ �a ./j / *a �a 0_ a 1�a 2la 3 4Y hO*�/a 0_ �a 5/l 6OPUO�*�,FOkj [OY��OjE�Oj*�,FOj*�,FOa 7*�,FOa 8*a ,FOP; �M@�L�KAB�J
�M .aevtoappnull  �   � ****@ k     �CC  ADD  uEE  �FF  �GG  ��I�I  �L  �K  A �H�H 0 thereel theReelB $ F�G z�F � ��E�D�C�B�A ��@ ��?�>�=�<�;�:�9�8�7�6�5 � ��4�3�2�1�0�/�.�-�,�G (0 unpackedfoldername unpackedFolderName
�F 
btns
�E 
dflt
�D 
cbtn
�C 
appr�B 
�A .sysodlogaskr        TEXT
�@ 
prmp
�? .sysostflalis    ��� null�> 0 	jobfolder 	jobFolder
�= 
cfol
�< 
alis�;  0 unpackedfolder unpackedFolder
�: 
cobj�9 0 thereels theReels
�8 
kocl
�7 .corecnte****       ****�6 0 thereelalias theReelAlias�5 $0 foldersubroutine folderSubRoutine�4 
�3 
brow
�2 
to  
�1 .corecrel****      � null�0 .0 newwindowprojectfiles newWindowProjectFiles
�/ ecvwclvw
�. 
pvew
�- .miscactvnull��� ��� obj 
�, .aevtoappnull  �   � ****�J ��E�O����lv�l�k�b   � 
O� [*��l E�O�a �/a &E` O*a _ /a -E` O ._ [a a l kh  �a &E` O)_ k+ OP[OY��UO� ;a �a kv�b   a  
Oa a �l E` Oa  _ a !,FO*j "OPUO*6j #��  ��  ascr  ��ޭ